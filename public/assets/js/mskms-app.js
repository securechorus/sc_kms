/* ===================================================
 * MIKEY SAKKE Key Management application specific scripts.
 * ========================================================== */

function loadUserList(current_user)
{
   var $content = $('#content');

   $content.load("/ui/user-list", function(){ //...

   var $add_user_form = $content.find('#add-user-form');
   var is_admin = !! $add_user_form.length;

   if (is_admin)
     $add_user_form.submit(function(e){
        var $this = $(this);
        $.ajax({
           url: this.action,
           type: 'POST',
           data: JSON.stringify({ name: $this.find('input[name=name]').val(),
                                  pass: $this.find('input[name=pass]').val() }),
           dataType: 'json',
           contentType: 'application/json',
           success: function(e) {
              $('.dropdown').removeClass('open');
              loadUserList(current_user);
           },
           error: function(xhr) {
              var body = $.parseJSON(xhr.responseText);
              $("<div class='alert alert-error'>\
                   <button class='close' data-dismiss='alert'>&times;</button>\
                   <strong>Error:</strong> "+body.error+"\
                 </div>").prependTo($this.closest('.dropdown-form'));
           }
        });
        return false; // prevent default submit behavior
     });

   var $update_ops =
       $("\
            <span class='btn-group'>\
            <a href='#' id='delete' class='btn btn-danger' ><i class='hidden-phone pull-left icon-remove'/> Delete </a> \
            </span>\
            <span class='btn-group'>\
            <a href='#' id='update' class='btn btn-primary'><i class='hidden-phone pull-left icon-ok'/> Update </a> \
            <a href='#' id='cancel' class='btn btn-cancel' ><i class='hidden-phone pull-left icon-thumbs-down'/> Cancel </a> \
            </span>\
       ")
       .on('click', function(){
          console.log("UID="+$(this).closest('tr').attr('data-uid'));
          console.log("Fetched URI="+$(this).closest('tr').attr('data-uri'));
          console.log("Edited URI="+$(this).closest('tr').find('input').val());
          // TODO: functionality!
       })
       ;

   var $add_ops = $("\
            <a href='#' id='add' class='btn btn-success'><i class='hidden-phone pull-left icon-plus'/> Add new URI </button> \
       ")
       .on('click', function(){
          var uid = $(this).closest('tr').attr('data-uid');
          var newuri = $(this).closest('tr').find('input').val() + '\0';
          var $err = $(this).closest('td');
          $.ajax({
              url: '/secure/1/user/'+uid+'/uri',
              type: 'POST',
              data: JSON.stringify({ uri: newuri }),
              dataType: 'json',
              contentType: 'application/json',
              success: function(e) {
                 // FIXME: could be much nicer than this
                 // FIXME: reload; just need to update
                 // FIXME: rows here really; this is
                 // FIXME: the cheapest solution though.
                 loadUserList(current_user);
              },
              error: function(xhr) {
                 // TODO: present errors better and more
                 // TODO: generically
                 var body = $.parseJSON(xhr.responseText);
                 $("<div class='alert alert-error'>\
                      <button class='close' data-dismiss='alert'>&times;</button>\
                      <strong>Error:</strong> "+body.error+"\
                    </div>").appendTo($err);
              }
          });
       })
       ;

   $.getJSON("/secure/1/user", function(data) 
   {
      var $tbody = $content.find('#users-table tbody');

      var reqs = [];

      $.each(data, function(i, user)
      {
         function make_row(uid, uri)
         {
            var $rc = $("<tr data-uid='"+user.id+"'/>");
            if (uri)
               $rc.attr('data-uri', uri);
            return $rc;
         }

         var $user_rows = make_row(user.id).append('<th>'+user.name+'</th>');

         var allow_edits = is_admin || user.name == current_user;

         reqs.shift($.getJSON("/secure/1/user/"+user.name+"/uri", function(user_uris) 
         {
            var $uri_row = $user_rows;

            if (user_uris.length > 0)
            {
               $user_rows.find('th:first').attr('rowspan', user_uris.length + allow_edits);

               $.each(user_uris, function(i, uri_data)
               {
                  if (i === 0)
                     $uri_row.attr('data-uri', uri_data.uri);
                  else
                     $uri_row = make_row(user.id, uri_data.uri).insertAfter($uri_row);

                  $('<td>'+uri_data.uri+'</td>').appendTo($uri_row);

                  if (allow_edits)
                     $("<td id='uri-ops'>X</td>").appendTo($uri_row);
                  else
                     $("<td class='muted'>None</td>").appendTo($uri_row);
               });

               if (allow_edits)
                  $uri_row = make_row(user.id).insertAfter($uri_row);
            }
            else if (!allow_edits)
               $('<td>Unspecified</td><td>None</td>').appendTo($uri_row.addClass('muted'));

            if (allow_edits)
            {
               var $uri_td = $("<td/>").appendTo($uri_row);
               var $uri_ops_td = $("<td id='uri-ops'/>").appendTo($uri_row);
               var $add_uri =
                  $("<input type='text' class='input fit' placeholder='Enter new URI...'>")
                  .appendTo($uri_td)
                  .closest('tr')
                  .bind('mouseover', function(){
                     $add_ops.detach().appendTo($uri_ops_td);
                  })
                  .bind('focus', function(){
                     $add_ops.detach().appendTo($uri_ops_td);
                  })
                  .bind('mouseleave', function(){
                     setTimeout(function(){
                        if ($uri_ops_td.get()[0] === $add_ops.parent().get()[0])
                           $add_ops.detach();
                     }, 200);
                  })
                  ;
            }
         }));

         $.when(reqs).done(function(){
            $tbody.append($user_rows);
         });
      });
   });

   }); // get() success
}

