use strict;
use POSIX;

# Return a timestamp in ISO8601 format.
sub create_timestamp
{
  my $time = shift;

  # If time not supplied use the current time.
  if (!defined $time)
  {
    $time = time();
  }

  # Generate and return ISO8601 time string.
  return strftime("%Y-%m-%dT%H:%M:%SZ", gmtime($time));
}

# Returns a unix timestamp and fraction from a 64-bit NTP timestamp.
sub parse_ntp_timestamp
{
  my $ntp_ts = shift;

  if ((length($ntp_ts) != 16) || ($ntp_ts !~ /[[:xdigit:]]+/))
  {
    print STDERR "Invalid NTP timestamp\n";
    return undef;
  }

  my %time_hash;
  $time_hash{'epoch'} = hex(substr($ntp_ts, 0, 8));
  $time_hash{'fraction'} = hex(substr($ntp_ts, 8, 8));
  return \%time_hash;
}

# convert a userURI to a userID using #format string supplied.
sub create_user_id
{
  my $user_uri = shift;
  my $format_str = shift;
  my $time = shift;
  my $kms_uri = shift;

  # Hash mapping conversion string to strftime formatter.
  my $tconv = {
    'year' => '%Y', 
    'month' => '%m',
    'yday' => '%j',
    'week' => '%W',
  };
      
  # Replace #uri, #user and #host parts with extracted content from URI. 
  if ($user_uri =~ /([\w\.\-]+)@([\w\.\-]+)/)
  {
    my $nai = "$1\@$2";
    my $user = $1;
    my $host = $2;

    $format_str =~ s/#uri/$nai/;
    $format_str =~ s/#user/$user/;
    $format_str =~ s/#host/$host/;
  }

  # Replace #parameters with parameter part of URI
  if ($user_uri =~ /\?(.*)$/)
  {
    my $params = $1;
    $format_str =~ s/#parameters/$params/;
  }

  # Replace time parts 
  for my $tname (keys(%{$tconv}))
  {
    my $tvalue = strftime($tconv->{$tname}, gmtime($time));
    $format_str =~ s/#$tname/$tvalue/;
  }

  # Replace #kms with supplied kms_uri. 
  $format_str =~ s/#kms/$kms_uri/;

  # Return the created userID.
  return $format_str;
}

# Extract a URI from a string.
sub get_uri
{
  my $uri = shift;

  # Validate URI (GUI appears to add a null on end)
  if ($uri =~ /^([:\w+\.\-]+@[\w\.\-]+)/)
  {
    $uri = $1;
    return $uri;
  }
  return undef;
}

# Count bits in a scalar value.
sub bitcount
{
  my $x = shift;
  my $rc = 0;
  while ($x != 0)
  {
    ++$rc;
    $x >>= 1;
  }
  return $rc;
}

# For inclusion
1;
