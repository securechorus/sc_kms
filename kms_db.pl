use strict;
use Mojolicious::Lite;

# https://bitbucket.org/abutcher/crypt-ecdsa-gmpz.git
use Crypt::ECDSA;
use Crypt::ECDSA::Util qw/bint hex_bint as_hex/;
use Crypt::OpenSSL::Random;
use Math::GMPz qw/Rmpz_invert/;
use Digest::SHA qw/sha1_base64/;
use Data::Dumper;
use DBI;
use YAML::XS qw/LoadFile Dump/;
    
require 'kms_utils.pl';

sub dbconnect
{ 
  DBI->connect("dbi:SQLite:sckms.db");
}

say "Initializing signing elliptic curve...";

my $sign_ec = Crypt::ECDSA::Curve::Prime->new(standard => 'ECP-256');

say "sign_ec->Gx:  ".as_hex($sign_ec->{G_x});
say "sign_ec->Gy:  ".as_hex($sign_ec->{G_y});
say "sign_ec->b:   ".as_hex($sign_ec->{b});
say "sign_ec->q:   ".as_hex($sign_ec->{q});

my %sakke_param_sets;
%sakke_param_sets =
(
   1 => { ## RFC 6509

         n    =>  128,

         p    =>  hex_bint (join '',
                  qw/997ABB1F 0A563FDA 65C61198 DAD0657A
                     416C0CE1 9CB48261 BE9AE358 B3E01A2E
                     F40AAB27 E2FC0F1B 228730D5 31A59CB0
                     E791B39F F7C88A19 356D27F4 A666A6D0
                     E26C6487 326B4CD4 512AC5CD 65681CE1
                     B6AFF4A8 31852A82 A7CF3C52 1C3C09AA
                     9F94D6AF 56971F1F FCE3E823 89857DB0
                     80C5DF10 AC7ACE87 666D807A FEA85FEB/),

         q    =>  hex_bint (join '',
                  qw/265EAEC7 C2958FF6 99718466 36B4195E
                     905B0338 672D2098 6FA6B8D6 2CF8068B
                     BD02AAC9 F8BF03C6 C8A1CC35 4C69672C
                     39E46CE7 FDF22286 4D5B49FD 2999A9B4
                     389B1921 CC9AD335 144AB173 595A0738
                     6DABFD2A 0C614AA0 A9F3CF14 870F026A
                     A7E535AB D5A5C7C7 FF38FA08 E2615F6C
                     203177C4 2B1EB3A1 D99B601E BFAA17FB/),

         Px   =>  hex_bint (join '',
                  qw/53FC09EE 332C29AD 0A799005 3ED9B52A
                     2B1A2FD6 0AEC69C6 98B2F204 B6FF7CBF
                     B5EDB6C0 F6CE2308 AB10DB90 30B09E10
                     43D5F22C DB9DFA55 718BD9E7 406CE890
                     9760AF76 5DD5BCCB 337C8654 8B72F2E1
                     A702C339 7A60DE74 A7C1514D BA66910D
                     D5CFB4CC 80728D87 EE9163A5 B63F73EC
                     80EC46C4 967E0979 880DC8AB EAE63895/),

         Py   =>  hex_bint (join '',
                  qw/0A824906 3F6009F1 F9F1F053 3634A135
                     D3E82016 02990696 3D778D82 1E141178
                     F5EA69F4 654EC2B9 E7F7F5E5 F0DE55F6
                     6B598CCF 9A140B2E 416CFF0C A9E032B9
                     70DAE117 AD547C6C CAD696B5 B7652FE0
                     AC6F1E80 164AA989 492D979F C5A4D5F2
                     13515AD7 E9CB99A9 80BDAD5A D5BB4636
                     ADB9B570 6A67DCDE 75573FD7 1BEF16D7/),

         g    =>  hex_bint (join '',
                  qw/66FC2A43 2B6EA392 148F1586 7D623068
                     C6A87BD1 FB94C41E 27FABE65 8E015A87
                     371E9474 4C96FEDA 449AE956 3F8BC446
                     CBFDA85D 5D00EF57 7072DA8F 541721BE
                     EE0FAED1 828EAB90 B99DFB01 38C78433
                     55DF0460 B4A9FD74 B4F1A32B CAFA1FFA
                     D682C033 A7942BCC E3720F20 B9B7B040
                     3C8CAE87 B7A0042A CDE0FAB3 6461EA46/),

         sha => 256, # sha-id
         hash_len => 32, # octets
   }
);

while (my ($id, $set) = each(%sakke_param_sets))
{
   $set->{E} = Crypt::ECDSA::Curve::Prime->new
   (
      standard => 'generic_prime',
      p => $set->{p},
      a => -3,
      b => 0,
   );
   $set->{P} = Crypt::ECDSA::Point->new
   (
      curve => $set->{E},
      X => $set->{Px},
      Y => $set->{Py},
   );
   die "P is not on curve for parameter set $id"
      if not $set->{P}->is_on_curve();
}

# hardcode to param set 1 (6509)
my $sakke_param_set_id = 1;
my $sakke_param_set = $sakke_param_sets{$sakke_param_set_id};

# cache public data
my $this_server_id;
my $Z;
my $KPAK;

# Check for first run for the server, with no database yet.
if (! -r "sckms.db")
{
   say "Creating database...";

   my $dbh = dbconnect();
   my $sql = <<ENDSQL;

   ------------ MIKEY-SAKKE Data ------------

   CREATE TABLE server_private_data
   (
      kmsSecretAuthenticationKey BLOB,     -- 6507 integer KSAK.
      kmsMasterSecret BLOB                 -- 6508 integer z.
   );
   CREATE TABLE public_certificate_data
   (
      id INTEGER PRIMARY KEY,              -- Internal key per certificate.
      Version VARCHAR(15) NOT NULL,        -- Certificate Version.
      Role VARCHAR(15) NOT NULL,           -- Certificate role (Root or External).
      CertUri VARCHAR(255) NOT NULL,       -- 6509 arbitrary string.
      KmsUri VARCHAR(255) NOT NULL,        -- 6509 arbitrary string.
      Issuer VARCHAR(255) NOT NULL,        -- Arbitrary string.
      ValidFrom VARCHAR(31),               -- Date from which the cert may be used.
      ValidTo VARCHAR(31),                 -- Data at which the cert expires.
      Revoked VARCHAR(7),                  -- Set non zero if cert revoked.
      UserIdFormat VARCHAR(255),           -- Format of UserID
      PubEncKey BLOB,                      -- 6508 ecpoint Z.
      PubAuthKey BLOB,                     -- 6507 ecpoint KPAK.
      sakkeParameterSetIndex INTEGER       -- SAKKE parameter set being used.
   );
   CREATE TABLE public_cert_cache
   (
      tokenId INTEGER PRIMARY KEY,         -- Cache token
      uri VARCHAR(255) NOT NULL,           -- User URI that added cache entry.
      expires TIMESTAMP                    -- Time at which token expires
   );
   CREATE TABLE domains
   (
      id INTEGER PRIMARY KEY,              -- Internal key per domain
      certId INTEGER,                      -- Id of certificate to which domain releates
      KmsDomain VARCHAR(255) NOT NULL      -- Arbitrary string.
   );
   CREATE TABLE transport_keys
   (
      id INTEGER PRIMARY KEY,              -- Internal key per Trk
      KeyName VARCHAR(255) NOT NULL,       -- Key name string.
      Key BLOB                             -- Binary key.
   );
   CREATE TABLE identities                 -- could identify a device,
   (                                       -- access-point/user-agent or user.
      userIdentifier BLOB                  -- user identifier: in 6709 this
         PRIMARY KEY,                      -- includes a datestamp;
      accountId INTEGER                    -- reference to login account
         REFERENCES accounts(id),          -- that created this data.
      userPublicValidationToken BLOB,      -- 6507 ecpoint PVT
      userSecretSigningKey BLOB,           -- 6507 integer SSK
      receiverSecretKey BLOB               -- 6508 ecpoint RSK
   );
   CREATE TABLE account_uris
   (
      id INTEGER REFERENCES accounts(id),  -- the account
      uri VARCHAR(240) PRIMARY KEY         -- a URI registered to the account
   );
   CREATE INDEX AccountURIs ON account_uris(id);
   CREATE INDEX ServerID ON public_certificate_data(KmsUri);

   ------------ User Authentication ------------

   CREATE TABLE this_server
   (
      whoami INTEGER                       -- ref to the server public root cert
         REFERENCES public_certificate_data(id),
      dbVersion INTEGER DEFAULT 1,         -- db version for migration
      kmsId VARCHAR(255),                  -- KMS server Id string
      certLife INTEGER DEFAULT 2592000     -- Root certificate life in seconds
   );
   CREATE TABLE accounts
   (
      id INTEGER PRIMARY KEY,              -- internal key.
      name VARCHAR(64) UNIQUE NOT NULL,    -- user name for https access.
      sha1 CHAR(20),                       -- SHA-1 of user's password.
      admin BOOLEAN,                       -- whether user has access
                                           -- to administrative functions
      trkId INTEGER                        -- reference to trk id 
         REFERENCES transport_keys(id)     -- associated with account
   );
   CREATE TABLE auth_sessions
   (
      sid VARCHAR(255) PRIMARY KEY,        -- auth session key
      accountId INTEGER                    -- user authenticated
         REFERENCES accounts(id),
      lastActivity TIMESTAMP               -- last auth activity 
         DEFAULT CURRENT_TIMESTAMP,
      clientData VARCHAR(255)              -- user-agent and ip address
   );
   CREATE INDEX AccountIdentities ON identities(accountId);
   CREATE INDEX AccountSessions ON auth_sessions(accountId);
ENDSQL

   for my $stm (split /(?<=\));/, $sql)
   {
      $dbh->do($stm) or die $dbh->errstr . " in " . $stm;
   }


   say "Building default certificates and users...  ";
   build_db_from_config($dbh, 'sckms_config.yaml');

   say "Done.";

   $dbh->disconnect();
}
else # cache public data
{
   refresh_public_cache();

   if (grep /^rekey$/, @ARGV)
   {
      # Rekey server and generate updated root certificate
      my $dbh = dbconnect();
      my $cert_life = scalar $dbh->selectrow_array('SELECT certLife FROM this_server') or return undef;
      community_rekey($dbh, $this_server_id, $cert_life);
   }
}


# Refresh the KMS server internal cache globals.  
sub refresh_public_cache
{
   my $dbh = dbconnect();

   $this_server_id = scalar $dbh->selectrow_array('SELECT whoami FROM this_server') or die $dbh->errstr;
   $KPAK = Crypt::ECDSA::Point->new(
            curve => $sign_ec,
            octet => pack('H*',$dbh->selectrow_array(
                        "SELECT PubAuthKey ".
                          "FROM public_certificate_data ".
                         "WHERE id = $this_server_id")));
   $Z = hex_bint $dbh->selectrow_array("SELECT PubEncKey ".
                                         "FROM public_certificate_data ".
                                        "WHERE id = $this_server_id");
   $sakke_param_set_id = $dbh->selectrow_array("SELECT sakkeParameterSetIndex ".
                                                 "FROM public_certificate_data ".
                                                "WHERE id = $this_server_id");
   $sakke_param_set = $sakke_param_sets{$sakke_param_set_id};

   $dbh->disconnect();
}


# build the database from a YAML config file
sub build_db_from_config
{
  my $dbh = shift;
  my $file = shift;

  my $config = LoadFile($file);
  #print Dump($config);

  # Check for mandatory parameters
  if (!exists($config->{'KmsName'}) || !exists($config->{'KmsId'}))
  {
    return undef;
  }
  
  for my $user (@{$config->{'Users'}})
  {
    my $name = $user->{'Name'};
    my $key = undef;
    my $admin = 0;
    
    if (exists($user->{'Admin'}))
    {
      $admin = $user->{'Admin'};
    }
    if (exists($user->{'Key'}))
    {
      $key = $user->{'Key'};
    }

    add_user_config($dbh, $user->{'Name'}, $user->{'Pass'}, $admin, $key);
 
    for my $uri (@{$user->{'Uris'}})
    {
      add_account_uri_by_user($dbh, $name, $uri);
    } 
  }

  for my $cert (@{$config->{'Certificates'}})
  {
    my $cname= "$cert->{'Name'}.$cert->{'DefaultDomain'}";
    my $kmsuri= "$config->{'KmsName'}.$cert->{'DefaultDomain'}";
    my @domains;

    # Add default and other doamins to domain list
    push(@domains, $cert->{'DefaultDomain'});
    push(@domains, @{$cert->{'OtherDomains'}});

    create_certificate($dbh, $cert->{'Role'}, $cname, $kmsuri, $config->{'KmsId'},
                       $cert->{'Life'}, $cert->{'UserIDFormat'}, \@domains);
  }

  return 1;
}

# Re-key the root certificate. Can also be used to re-key external certificates, but this is only for
# library test. This KMS currently does not support true external certificates.
sub community_rekey
{
   my $dbh = shift;
   my $cert_id = shift;
   my $time_length = shift;

   say "Community rekey: Generating new MIKEY-SAKKE secrets and corresponding public data...";

   my $AuthKey = Crypt::ECDSA::Key->new(
         curve => $sign_ec,
         X => $sign_ec->{G_x},
         Y => $sign_ec->{G_y},
       # d => bint 0x12345,  ## XXX:TEST RFC6507 example override
      );

   my $KSAK = $AuthKey->secret;
   my $q = $sakke_param_set->{q};

   my $z;
   {
      my $qw = bitcount($q-1);
      say $qw;
      for (;;) # probably unnecessary
      {
         my $zr = Crypt::ECDSA::Util::random_bits($qw);
         $z = $zr % $q;
         last # if $zr != $z;
      }
   } 

   # XXX:TEST overwrite random with RFC6508 example
   # $z = hex_bint join '', qw/AFF429D3 5F84B110 D094803B 3595A6E2 998BC99F/;

   # If we are updating the root cert 
   if ($cert_id == $this_server_id)
   {
     # cache in global
     $KPAK = $AuthKey->Q;
     $Z = $sakke_param_set->{P} * $z;

     open KEYLOG, ">>keys.log";
     for my $file (\*STDERR, \*KEYLOG)
     {
       say $file "Authentication Keys...";

       say $file "G:         ".unpack('H*', $AuthKey->G->to_octet);
       say $file "KSAK:      ".as_hex($KSAK);
       say $file "KPAK:      ".unpack('H*', $KPAK->to_octet);
       say $file "CHECK:     ".unpack('H*', ($AuthKey->G * $AuthKey->secret)->to_octet);

       say $file "SAKKE Keys...";

       say $file "n:         ".$sakke_param_set->{n};
       say $file "p:         ".as_hex($sakke_param_set->{p});
       say $file "q:         ".as_hex($sakke_param_set->{q});
       say $file "Px:        ".as_hex($sakke_param_set->{P}->X);
       say $file "Py:        ".as_hex($sakke_param_set->{P}->Y);
       say $file "P->order:  ".as_hex($sakke_param_set->{P}->order);
       say $file "g:         ".as_hex($sakke_param_set->{g});
       say $file "sha:       ".$sakke_param_set->{sha};
       say $file "hash_len:  ".$sakke_param_set->{hash_len};
       say $file "z:         ".as_hex($z);
       say $file "Z:         ".unpack('H*', $Z->to_octet);
     }
     close KEYLOG;

     $dbh->do('DELETE FROM identities');
     $dbh->do('DELETE FROM server_private_data'); 
     $dbh->do('DELETE FROM public_cert_cache');
  
     $dbh->do(<<ENDSQL,undef,(substr(as_hex($KSAK),2), substr(as_hex($z),2)));
        INSERT INTO server_private_data VALUES (?, ?);
ENDSQL
   }
   
   # Get times for certificate update
   my $timenow = time();
   my $expire = $timenow + $time_length;

   $dbh->do("UPDATE public_certificate_data SET ValidFrom = '$timenow', ValidTo = '$expire'," .
            "Revoked = 'false' WHERE id = $cert_id");

   $dbh->do(<<ENDSQL,undef,(unpack('H*', $KPAK->to_octet), unpack('H*', $Z->to_octet)));
      UPDATE public_certificate_data
         SET PubAuthKey = ?,
             PubEncKey = ?
       WHERE id = $cert_id
ENDSQL
}

# Returns true if account ID is a database administrator.
sub is_admin_account
{
  my $dbh = shift;
  my $accountId = shift;

  $accountId = get_account_from_session($accountId) if ref $accountId;
  return scalar $dbh->selectrow_array(
      "SELECT admin FROM accounts WHERE id = $accountId");
}

# Find the account ID for a user URI.
sub get_account_id_from_uri
{
  my $dbh = shift;
  my $user_uri = shift;

  # Get account from user id
  my $accountId = scalar $dbh->selectrow_array(
    "SELECT id FROM account_uris WHERE uri = '$user_uri'") or return undef;
  
  return $accountId;
}

# Render a error in JSON.
sub json_error
{
   ({error => shift}, status => shift // 500);
}

# Get the KSAK server private key.
sub get_KSAK
{
   return hex_bint app->dbh->selectrow_array(
      "SELECT kmsSecretAuthenticationKey FROM server_private_data");
}

# Get the server master secret.
sub get_master_secret
{
   return hex_bint app->dbh->selectrow_array(
      "SELECT kmsMasterSecret FROM server_private_data");
}

# Create and add an new certificate to the database.
sub create_certificate
{
  my $dbh = shift;
  my $role = shift;
  my $cert_identity = shift;
  my $kms_identity = shift;
  my $issuer = shift;
  my $cert_life = shift;
  my $user_id_format = shift;
  my $domains_array = shift;

  if (($role ne 'Root') && ($role ne 'External'))
  { 
    die 'Invalid certificate type';
  }
  
  $dbh->do("INSERT INTO public_certificate_data " . 
           "(Version, Role, CertUri, KmsUri, Issuer, sakkeParameterSetIndex) ".
           "VALUES ('1.0.0', '$role', '$cert_identity', '$kms_identity', '$issuer', '$sakke_param_set_id')") or return $dbh->errstr;
 
  if(defined($user_id_format))
  {
     $dbh->do("UPDATE public_certificate_data SET UserIdFormat = '$user_id_format'");
  }

  my $cert_id = scalar $dbh->selectrow_array(
           "SELECT id FROM public_certificate_data ".
           "WHERE KmsUri = '$kms_identity'") or return $dbh->errstr;

  for my $domain (@{$domains_array})
  {  
    $dbh->do("INSERT INTO domains (certId,  KmsDomain) " .
             "VALUES ('$cert_id', '$domain')") or return $dbh->errstr;
  }

  # If creating the root, set the global id
  if ($role eq 'Root')
  {
    $this_server_id = $cert_id;
    $dbh->do("INSERT INTO this_server (whoami, kmsId, certLife) VALUES ('$cert_id', '$issuer', '$cert_life')") or return $dbh->errstr;
  }
  
  community_rekey($dbh, $cert_id, $cert_life);

  return 0;
}

# Add a new user to the database from parameters given in the first start config file.
sub add_user_config
{
  my $dbh = shift;
  my $name = shift;
  my $pass = shift;
  my $admin = shift;
  my $trk = shift; # Optional
 
  my $new_key = create_boot_trk($dbh, $name, $trk);

  my $trk_id = scalar $dbh->selectrow_array(
          "SELECT id FROM transport_keys ".
          "WHERE KeyName = '$new_key->{'KeyName'}'") or return json_error $dbh->errstr;   

  $dbh->do(<<ENDSQL,undef,($name, sha1_base64($pass), $trk_id, !!$admin)) or return json_error $dbh->errstr;
     INSERT INTO accounts (name, sha1, trkId, admin) VALUES (?, ?, ?, ?)
ENDSQL

  return 1;
}

# Get the KMS ID name 
sub get_kms_id
{
  my $dbh = shift;

  return (scalar $dbh->selectrow_array('SELECT kmsId FROM this_server') or undef);
}

# Check KMS domain is valid 
sub check_kms_domain
{
  my $dbh = shift;
  my $kms_uri = shift;

  my $cert = get_root_cert($dbh) or return undef;

  if (lc($cert->{'KmsUri'}) eq lc($kms_uri))
  {
    return 1;
  }
  return undef;
}

# Get the KMS domain name.
sub get_kms_domain
{
  my $dbh = shift;
  my $kms_uri = shift;

  my $cert = get_root_cert($dbh) or return undef;

  return $cert->{'KmsUri'};
}

# Obtain a key pack for a user URI. The parameters user_uri and start_time are optional.
# If user_uri is supplied, keys only for this user_uri are supplied. If not supplied
# keys fro the requesting user and all aliases are supplied. If start_time is supplied
# keys will be generated from request start time.
sub get_key_material
{
  my $dbh = shift;
  my $rq_user_uri = shift;
  my $user_uri = shift;
  my $start_time = shift;

  my @uris;

  # Find the acount ID that maps to the requesting user URI.
  my $account_id = get_account_id_from_uri($dbh, $rq_user_uri) or return undef;
  if (defined($user_uri))
  {
    if (get_account_id_from_uri($dbh, $user_uri) != $account_id)
    {
      return undef;
    }
    push(@uris, $user_uri);
  }
  else
  {
    # Get all URIs for requesting URI.
    my $uris_ref = app->dbh->selectall_arrayref(
       "SELECT uri FROM account_uris WHERE id = $account_id") or return undef;

    for my $uri (@{$uris_ref})
    {
      push(@uris, $uri->[0]);
    }
  }

  my @all_key_mat;

  for my $uri (@uris)
  {
    my %key_material;
  
    say "Key provision, rq URI $rq_user_uri for user URI $uri";
    
    # Get certificate for user. FIXME What to do if user not in our root domain
    my $cert = get_root_cert($dbh);

    my $key_time = time();
    if (defined($start_time))
    { 
      $key_time = $start_time;
    }

    my $userIdentifier = create_user_id($uri, $cert->{'UserIdFormat'}, $key_time, $cert->{'KmsUri'});

    # ===== Compute signing keys. =====================

    my $AuthKey = Crypt::ECDSA::Key->new(
        curve => $sign_ec,
        X => $sign_ec->{G_x},
        Y => $sign_ec->{G_y},
        # d => bint 0x23456,  ## XXX:TEST RFC6507 example override
    );

    my $PVT = $AuthKey->G * $AuthKey->secret;

    my $hash = new Digest::SHA($sakke_param_set->{sha});

    # XXX:TEST vector from 6507
    # $userIdentifier = "2011-02\0tel:+447700900123\0";

    say "G    := " . unpack('H*', $AuthKey->G->to_octet);
    say "KPAK := " . unpack('H*', $KPAK->to_octet);
    say "ID   := " . $userIdentifier;
    say "PVT  := " . unpack('H*', $PVT->to_octet);

    $hash->add($AuthKey->G->to_octet);
    $hash->add($KPAK->to_octet);
    $hash->add($userIdentifier);
    $hash->add($PVT->to_octet);

    my $HS = hex_bint $hash->hexdigest;

    my $KSAK = get_KSAK;

    my $SSK = ($KSAK + $HS * $AuthKey->secret) % $AuthKey->G->order; 

    my $SSK_hex = substr(as_hex($SSK),2);
    my $PVT_hex = unpack('H*', $PVT->to_octet);

    say "$userIdentifier: PVT = " . $PVT_hex;
    say "$userIdentifier: HS  = " . as_hex($HS);
    say "$userIdentifier: SSK = " . $SSK_hex;

    # ===== Compute receiver secret. =====================

    my $z = get_master_secret;

    my $a = hex_bint unpack 'H*', $userIdentifier;
    my $RSK = $a + $z;
    Rmpz_invert($RSK, $RSK, $sakke_param_set->{q});
    $RSK = $sakke_param_set->{P} * $RSK;
    my $RSK_hex = unpack('H*', $RSK->to_octet);

    say "$userIdentifier: RSK = " . $RSK_hex;

    # Add private key material to return hash, pad to nearest 8 bytes where required.
    # Note removal 04 header on RSK and PVT
    $key_material{'private'}{'UserDecryptKey'} = pack('H*', unpack('x1 H*', $RSK->to_octet));
    $key_material{'private'}{'UserSigningKeySSK'} = pack('H*', $SSK_hex);
    $key_material{'private'}{'UserPubTokenPVT'} = pack('H*', unpack('x1 H*', $PVT->to_octet));

    # Populate some information initially from the user certificate
    for my $item ('KmsUri', 'CertUri', 'Issuer', 'ValidTo', 'Revoked')
    {
      $key_material{$item} = $cert->{$item};
    }

    # Add in user info and valid from time
    $key_material{'UserUri'} = $uri;
    $key_material{'UserID'} = $userIdentifier;
    $key_material{'ValidFrom'} = $key_time;
  
    # Delete previous entry and reinsert
    $dbh->do(<<ENDSQL,undef,($userIdentifier));
         DELETE FROM identities
          WHERE userIdentifier = ?
ENDSQL

    # Add key material
    $dbh->do(<<ENDSQL,undef,($userIdentifier, $account_id, $PVT_hex, $SSK_hex, $RSK_hex));
          INSERT INTO identities (userIdentifier,
                                  accountId,
                                  userPublicValidationToken,
                                  userSecretSigningKey,
                                  receiverSecretKey)
          VALUES (?, ?, ?, ?, ?)
ENDSQL

    push(@all_key_mat, \%key_material);
  }

  return \@all_key_mat;
}

# Creates a new TrK and adds to database.
sub create_trk
{
   my $dbh = shift;
   my $name = shift;
   my $trk = shift;

   if (!defined($trk))
   {
     # Generate 256bit key
     $trk = Crypt::OpenSSL::Random::random_bytes(32);
   }

   # Create new keyname
   my $trk_name = scalar($dbh->selectrow_array("SELECT COALESCE(MAX(id), 0) FROM transport_keys")) + 1;
   $trk_name = "tk.$trk_name.$name";

   # Store in database as a hex string
   my $trk_hex = unpack('H*', $trk);   

   say "New Trk '$trk_name' = $trk_hex";
   $dbh->do("INSERT INTO transport_keys (KeyName, Key) VALUES ('$trk_name', '$trk_hex')") or return undef;

   # Return a TrK key hash
   return { 'KeyName' => $trk_name, 'Key' => $trk };
}

# Create TRK and writes to file to enable bootstrap of a client.
sub create_boot_trk
{
   my $dbh = shift;
   my $name = shift;
   my $trk = shift;
   
   my $new_key = create_trk($dbh, "$name.boot.key", $trk) or return json_error 'Failed to create Trk';
   
   say "Boot key for '$name' written to '$name.boot.key'";

   # Dump out the bootstrap key for user
   if(open(my $fh, '>', "$name.boot.key"))
   {
     print $fh unpack('H*', $new_key->{'Key'}) . "\n";
     print $fh $new_key->{'KeyName'} . "\n";
     close ($fh);
   }   

   return $new_key;
}


# Removes a Trk from the database.
sub remove_trk
{
  my $dbh = shift;
  my $name = shift;

  my $removed = $dbh->do("DELETE FROM transport_keys WHERE KeyName = '$name'");

  return $removed;
}

# Lookup TrK for user.
sub lookup_trk_by_user_uri
{
  my $dbh = shift;
  my $user_uri = shift;
 
  my $accountId = get_account_id_from_uri($dbh, $user_uri) or return undef;

  my $trkId = scalar $dbh->selectrow_array(
    "SELECT trkId FROM accounts WHERE id = '$accountId'") or return undef;

  my $trk = $dbh->selectall_arrayref(
    "SELECT * FROM transport_keys WHERE id = '$trkId'", { Slice => {} }) or return undef;
  
  # Convert key back form hex to binary form
  $trk->[0]{'Key'} = pack('H*', $trk->[0]{'Key'});

  return $trk->[0];
}

# Obtain the root certificate.
sub get_root_cert
{
  my $dbh = shift;

  my $certs = $dbh->selectall_arrayref(
    "SELECT * FROM public_certificate_data WHERE id = $this_server_id", { Slice => {} }) or return undef;
   
  # Obtain the domains associated with the certificate
  my $domains = $dbh->selectall_arrayref(
    "SELECT * FROM domains WHERE certId = $this_server_id", { Slice => {} }) or return undef;
 
  # Add in the domains for the certificate
  $certs->[0]{'domains'} = $domains;

  return $certs->[0];
}

# Obtain an array of External certificates.
sub get_external_certs
{
  my $dbh = shift;

  my $certs = $dbh->selectall_arrayref(
    "SELECT * FROM public_certificate_data WHERE Role = 'External'", { Slice => {} }) or return undef;
  
  for my $cert (@{$certs})
  {
    # Obtain the domains associated with the certificate
    my $domains = $dbh->selectall_arrayref(
      "SELECT * FROM domains WHERE certId = $cert->{'id'}", { Slice => {} }) or return undef;
    
    # Add in the domains for the certificate
    $cert->{'domains'} = $domains;
  }
  return $certs;
}

# Obtain an array of certificates.
sub get_all_certs
{
  my $dbh = shift;

  my $certs = $dbh->selectall_arrayref(
    "SELECT * FROM public_certificate_data", { Slice => {} }) or return undef;
  
  for my $cert (@{$certs})
  {
    # Obtain the domains associated with the certificate
    my $domains = $dbh->selectall_arrayref(
      "SELECT * FROM domains WHERE certId = $cert->{'id'}", { Slice => {} }) or return undef;
    
    # Add in the domains for the certificate
    $cert->{'domains'} = $domains;
  }
  return $certs;
}

# Create an new certificate cache token.
sub create_cert_cache_token
{
  my $dbh = shift;
  my $uri = shift;
  my $expire_time = shift;

  $dbh->do("INSERT INTO public_cert_cache (uri, expires) VALUES ('$uri', '$expire_time')");

  return $dbh->last_insert_id("", "", "", "");
}

# Create an new certificate cache token.
sub get_cert_cache_token
{
  my $dbh = shift;
  my $uri = shift;
  my $token_id = shift;

  my $expire_time = scalar $dbh->selectrow_array(
            "SELECT expires FROM public_cert_cache ".
            "WHERE tokenId = '$token_id'");
  return 0 if not defined $expire_time; 

  if ($expire_time > time())
  {
    # Cache is still valid
    return 1;
  }  

  # Cache has expired
  $dbh->do("DELETE FROM public_cert_cache WHERE tokenId = '$token_id' AND uri = '$uri'");
  return 2;
}

# Adds a new URI to a user in the database.
sub add_account_uri_by_user
{
  my $dbh = shift;
  my $account = shift;
  my $uri = shift;
  
  # Create an new URI for an server user
  my $account_id = scalar $dbh->selectrow_array(
            "SELECT id FROM accounts ".
            "WHERE name = '$account'") or return json_error $dbh->errstr;

  # Create boot Trk key
  return db_add_account_uri($dbh, $account_id, $uri);
}

# Update the Trk associated with a user URI
sub update_account_trk_from_uri
{
  my $dbh = shift;
  my $trk_name = shift;
  my $user_uri = shift;
  
  my $account_id = get_account_id_from_uri($dbh, $user_uri) or return undef;

  my $key_id = scalar $dbh->selectrow_array(
    "SELECT id FROM transport_keys WHERE KeyName = '$trk_name'") or return undef;

  my $updated = $dbh->do("UPDATE accounts SET trkId = '$key_id' WHERE id = '$account_id'");

  return $updated;  
}

# Adds a new user account to the database.
sub add_user
{
   my $dbh = shift;
   my $accountId = shift;
   my $name = shift;
   my $pass = shift;
   my $admin = 0;

   return json_error "Must be an administrator to add users.", 403 if not is_admin_account($dbh, $accountId);     
   return add_user_config($dbh, $name, $pass, $admin); 
}

# Get database account from GUI session ID.
sub get_account_from_sid
{
   my $dbh = shift;
   my $sid = shift;

   return scalar $dbh->selectrow_array(<<ENDSQL,undef,($sid));
      SELECT accountId FROM auth_sessions WHERE sid = ?
ENDSQL
}

# Find the account mapped to a user name or UID.
sub resolve_to_account
{
   my $dbh = shift;
   my $name_or_uid = shift;

   return scalar $dbh->selectrow_array(<<ENDSQL,undef,($name_or_uid));
      SELECT id FROM accounts WHERE name = :1 OR id = :1
ENDSQL
}

# Adds a URI to a user account.
sub db_add_account_uri
{
   my $dbh = shift; 
   my $accountId = shift;
   my $uri = shift;
  
   # Ensure input URI is ok
   $uri = get_uri($uri);

   say "Add account URI, Account ID $accountId, URI $uri";

   return json_error "No 'uri' parameter specified", 400 if not $uri; 
   
   $dbh->do(<<ENDSQL,undef,($accountId, $uri)) or return json_error $dbh->errstr;
      INSERT INTO account_uris (id, uri) VALUES (?, ?)
ENDSQL

   return ({success => "URI '$uri' added to account '$accountId'"}, status => 201);
}

# Modifies a URI associated a user account.
sub db_update_account_uri
{
   my $dbh = shift; 
   my ($accountId, $from_uri, $to_uri) = @_;

   return "No 'uri' query parameter specified" if not $from_uri;
   return "No 'uri' field specified in body" if not $to_uri;

   return {success => "No change requested."};

   my $updated = $dbh->do(<<ENDSQL,undef,($to_uri, $accountId, $from_uri));
      UPDATE account_uris SET uri = ? WHERE id = ? AND uri = ?
ENDSQL
   return json_error $dbh->errstr if not $updated;
   return json_error "URI '$from_uri' not associated with account '$accountId'", 404 if not 0+$updated;

   {success => "URI '$from_uri' updated to '$to_uri' for account '$accountId'"}
}

# Removes a URI form a user account.
sub db_remove_account_uri
{
   my $dbh = shift; 
   my $accountId = shift;
   my $uri = shift;

   my $removed = $dbh->do(<<ENDSQL,undef,($accountId, $uri));
      DELETE FROM account_uris
       WHERE id = ?
         AND uri = ?
ENDSQL
   return json_error $dbh->errstr if not $removed;
   return json_error "URI '$uri' not associated with account '$accountId'", 404 if not 0+$removed;
   {success => "URI '$uri' removed from account '$accountId'"}
}

# For inclusion
1; 
