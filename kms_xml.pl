use strict;

require "w3c_xml.pl";
require "kms_utils.pl";

use XML::LibXML;
use XML::LibXML::XPathContext;

# The namespace used by the KMS XML
use constant KMS_XML_SE_NAMESPACE => 'http://www.cesg.gov.uk/SecureChorus';
use constant KMS_XML_KM_NAMESPACE => 'urn:3GPP:ns:PublicSafety:KeyManagement:2014';
use constant XML_SCHEMA_NAMESPACE => 'http://www.w3.org/2001/XMLSchema-instance';
use constant XML_DSIG_NAMESPACE => 'http://www.w3.org/2000/09/xmldsig#';

# Global containing the Schema used for validation
my $kms_xml_schema = XML::LibXML::Schema->new( location => 'schemas/SE_KmsInterface_XMLSchema.xsd' );

# Return the KMS XML namespace as a string.
sub get_kms_namespace
{
  return KMS_XML_SE_NAMESPACE;
}

# Load and validate an XML text document.
sub load_validated_xml
{
  my $xml_text = shift;

  if (!defined($kms_xml_schema))
  {
    return undef;
  }

  my $dom;

  # Load and validate in an eval to catch any exceptions
  # from the load and validation of the XML.
  eval
  {
     $dom = XML::LibXML->load_xml(string => $xml_text, clean_namespaces => 1);

     print $dom->toString(1) . "\n";

     $kms_xml_schema->validate( $dom );
  };

  # Check if an exception has occured
  if ($@)
  {
    print STDERR "XML Validate error : $@\n";
    return undef;
  }

  return $dom;
}

# Validate a DOM xml document.
sub validate_dom_xml
{
  my $dom = shift;
  $kms_xml_schema->validate($dom);
}

# Find an XML node list and parse to a Perl hash.
sub parse_xml_to_hash
{
  my $rq_dom = shift;
  my $namespace = shift;
  my $element = shift;
  my $data = shift;

  my $xpath_ctx = XML::LibXML::XPathContext->new($rq_dom);
  $xpath_ctx->registerNs('ns' => $namespace);
  
  my $nodes = $xpath_ctx->findnodes("//ns:$element");
  
  if ($nodes->size()) 
  {
    for my $node ($nodes->get_node(0)->getChildNodes())
    {
      if ($node->nodeType == XML_ELEMENT_NODE)
      {
        $data->{$node->nodeName} = $node->textContent;
      }
    }
    return 1;
  }
  return undef;
}

# Create a new empty KMS XML response.
sub create_kms_response_xml
{
  my $rq_response = shift;
  my $error = shift;

  my $doc = XML::LibXML::Document->new( '1.0', 'UTF-8' );

  # Create the root node and assign to document
  my $root = $doc->createElementNS(KMS_XML_SE_NAMESPACE, 'SignedKmsResponse' );
  $doc->setDocumentElement($root);

  # Add its namespaces and attributes
  $root->setNamespace(XML_SCHEMA_NAMESPACE, 'xsi', 0);
  $root->setNamespace(XML_DSIG_NAMESPACE , 'ds', 0);
  $root->setNamespace(KMS_XML_SE_NAMESPACE, 'se', 0);
  my $sl = $doc->createAttributeNS(XML_SCHEMA_NAMESPACE,
    'schemaLocation' => (KMS_XML_SE_NAMESPACE . ' SE_KmsInterface_XMLSchema.xsd'));
  $root->addChild($sl);

  # Create response element and add attributes
  my $response = $doc->createElementNS(KMS_XML_KM_NAMESPACE, 'KmsResponse');
  $root->appendChild($response);
  $response->addChild($doc->createAttribute('Id' => 'xmldoc'));
  $response->addChild($doc->createAttribute('Version' => '1.0.0'));

  # Add the response elements
  for my $el_name ('KmsUri', 'UserUri', 'Time', 'KmsId', 'ClientReqUrl')
  {
    if (exists($rq_response->{$el_name}))
    {
      $response->appendTextChild($el_name, $rq_response->{$el_name}); 
    }
  }

  my $mesg;

  if (!defined($error))
  {
    # Create an empty KMS Message body
    $mesg = $doc->createElementNS(KMS_XML_KM_NAMESPACE, 'KmsMessage');
    $response->appendChild($mesg);
  }
  else
  {
    # Create an empty KMS Error body
    $mesg = $doc->createElementNS(KMS_XML_KM_NAMESPACE, 'KmsError');
    $response->appendChild($mesg);
  }

  # Return the document and the document message node
  return ($doc, $mesg);
}

# Create a generic KMS Type XML fragment.
sub create_kms_body_type_xml
{
  my $doc = shift;
  my $node = shift;
  my $name = shift;
  my $type = shift;

  # Create the KMS type body
  my $body_node = $doc->createElementNS(KMS_XML_KM_NAMESPACE, $name);
  $node->appendChild($body_node); 

  # Add Attributes type attributes to the body
  $body_node->addChild($doc->createAttribute('Version' => '1.0.0'));
  if ($type)
  {
    $name = "se:$name" . 'TkType';
    my $attrib = $doc->createAttributeNS(XML_SCHEMA_NAMESPACE, 'type' => $name);
    $body_node->addChild($attrib);
  }

  return $body_node;
}

# Create KMS certificate XML.
sub create_cert_xml
{
  my $doc = shift;
  my $node = shift;
  my $cert_hash = shift;
  my $sig_id = shift;
  
  my $kms_cert = $doc->createElementNS(KMS_XML_KM_NAMESPACE, 'KmsCertificate');
  $node->appendChild($kms_cert);

  # Add the attributes to the certificate element
  for my $at_name ('Version', 'Role')
  {
    $kms_cert->addChild($doc->createAttribute($at_name => $cert_hash->{$at_name}));
  }

  if (defined($sig_id))
  {
    $kms_cert->addChild($doc->createAttribute('Id' => $sig_id));
  }

  # Add the certificate elements
  for my $el_name ('CertUri', 'KmsUri', 'Issuer', 'ValidFrom', 'ValidTo',
                  'Revoked', 'UserIdFormat', 'PubEncKey', 'PubAuthKey')
  {
    if (exists($cert_hash->{$el_name}))
    {
      # Print timestamps as strings
      if ($el_name =~ /^Valid/)
      {
        $kms_cert->appendTextChild($el_name, create_timestamp($cert_hash->{$el_name})); 
      }
      else
      {
        $kms_cert->appendTextChild($el_name, $cert_hash->{$el_name}); 
      }

      # The User format has an attribute
      if ($el_name eq 'UserIdFormat')
      {
        $kms_cert->lastChild->addChild($doc->createAttribute('Conversion' => 'Hash'));
      }
    }
  }

  # Add the domain list
  my $domain_list = $doc->createElementNS(KMS_XML_KM_NAMESPACE, 'KmsDomainList');
  $kms_cert->appendChild($domain_list);
  for my $domain_hash (@{$cert_hash->{'domains'}})
  {
    $domain_list->appendTextChild('KmsDomain', $domain_hash->{'KmsDomain'}); 
  }
}

# Create KMS key type XML.
sub create_key_type_xml
{
  my $doc = shift;
  my $node = shift;
  my $name = shift;

  # Add key type element and attributes
  my $key_type = $doc->createElementNS(KMS_XML_KM_NAMESPACE, $name);
  $node->appendChild($key_type); 

  my $attrib = $doc->createAttributeNS(XML_SCHEMA_NAMESPACE,
                                       'type' => 'se:EncKeyContentType');
  $key_type->addChild($attrib);

  return $key_type;
}

# Create KMS key set XML.
sub create_key_set_xml
{
  my $doc = shift;
  my $node = shift;
  my $key_mat = shift;
  my $enc_key = shift;
  
  my $key_set = $doc->createElementNS(KMS_XML_KM_NAMESPACE, 'KmsKeySet');
  $node->appendChild($key_set);
  $key_set->addChild($doc->createAttribute('Version' => '1.0.0'));

  # Add the keyset elements
  for my $el_name ('KmsUri', 'CertUri', 'Issuer', 'UserUri', 'UserID',
                   'ValidFrom', 'ValidTo', 'Revoked')
  {
    # Print timestamps as strings
    if ($el_name =~ /^Valid/)
    {
      $key_set->appendTextChild($el_name, create_timestamp($key_mat->{$el_name})); 
    }
    else
    {
      $key_set->appendTextChild($el_name, $key_mat->{$el_name}); 
    }
  }
   
  if (exists($key_mat->{'private'}))
  { 
    my $enc_key_name = $enc_key->{'KeyName'};
    for my $key_name ('UserDecryptKey', 'UserSigningKeySSK', 'UserPubTokenPVT')
    {
      my $key_type = create_key_type_xml($doc, $key_set, $key_name);
      my $key_data = $key_mat->{'private'}{$key_name};
     
      #print STDERR "KEY Length " . length($key_data) . "DATA " . $key_data . "\n"; 
      encypted_key_xml($doc, $key_type, $key_data, "",
                      $enc_key->{'Key'}, $enc_key_name);

      # Only supply key name for first               
      $enc_key_name = "";            
    }
  }
}

# Create a KMS new transport key XML fragment.
sub create_new_trk_xml
{
  my $doc = shift;
  my $node = shift;
  my $trk_old = shift;
  my $trk_new = shift;

  # Add the new TrK
  my $new_trk = $doc->createElementNS(KMS_XML_SE_NAMESPACE, 'NewTransportKey');
  $node->appendChild($new_trk); 

  encypted_key_xml($doc, $new_trk, 
                   $trk_new->{'Key'}, $trk_new->{'KeyName'},
                   $trk_old->{'Key'}, $trk_old->{'KeyName'});
}

# Create KMS Init response XML.
sub create_kms_init_response
{
  my $rq_response = shift;
  my $cert = shift;
  my $trk_old = shift;
  my $trk_new = shift;

  # Create empty response XML and add content.
  (my $doc, my $mesg_node) = create_kms_response_xml($rq_response); 

  # Create the KMSInit body
  my $kms_init = create_kms_body_type_xml($doc, $mesg_node, 'KmsInit', 1);

  # Add the root certificate
  create_cert_xml($doc, $kms_init, $cert);
  
  # Add the new TrK
  create_new_trk_xml($doc, $kms_init, $trk_old, $trk_new);

  if (sign_xml($doc, KMS_XML_KM_NAMESPACE, 'xmldoc',
               $trk_old->{'Key'}, $trk_old->{'KeyName'}, 128))
  {
    return $doc;
  }
}

# Create KMS Key Provision response XML.
sub create_kms_key_prov_response
{
  my $rq_response = shift;
  my $key_mat_array = shift;
  my $trk_old = shift;
  my $trk_new = shift;
    
  my $trk_enc = $trk_new;

  # If new transport key not supplied use the old
  if (!defined($trk_new))
  {
    $trk_enc = $trk_old;
  }
    $trk_enc = $trk_old;

  # Create empty response XML and add content.
  (my $doc, my $mesg_node) = create_kms_response_xml($rq_response);
 
  # Create the KMS Key Prov body
  my $kp = create_kms_body_type_xml($doc, $mesg_node, 'KmsKeyProv', 1);
  
  for my $key_mat (@{$key_mat_array})
  {
    # Add provisioned keys
    create_key_set_xml($doc, $kp, $key_mat, $trk_enc);
  }
  
  # If supplied add new transport key
  if (defined($trk_new))
  {
    create_new_trk_xml($doc, $kp, $trk_old, $trk_new);
  }

  # Sign the generated XML
  if (sign_xml($doc, KMS_XML_KM_NAMESPACE, 'xmldoc',
              $trk_old->{'Key'}, $trk_old->{'KeyName'}, 128))
  { 
    return $doc;
  }
}

# Create KMS Cert cache response XML.
sub create_kms_cert_cache_response
{
  my $request = shift;
  my $cert_array = shift;
  my $trk_enc = shift;
  my $cache_num = shift;

  # Create empty response XML and add content.
  (my $doc, my $mesg_node) = create_kms_response_xml($request);
  
  my $kms_cc = create_kms_body_type_xml($doc, $mesg_node, 'KmsCertCache', 0);
 
  # Add cache num if defined
  if (defined($cache_num))
  {
    my $cn_attrib = $doc->createAttribute('CacheNum' => $cache_num);
    $kms_cc->addChild($cn_attrib); 
  }

  # Add the certificates
  my $cert_no = 1;
  for my $cert (@{$cert_array})
  {
    my $signed_kms_cert = $doc->createElementNS(KMS_XML_KM_NAMESPACE, 'SignedKmsCertificate');
    $kms_cc->appendChild($signed_kms_cert); 

    create_cert_xml($doc, $signed_kms_cert, $cert, "xmlcache$cert_no");
  
    # Sign the generated XML
    if (!sign_xml($doc, KMS_XML_KM_NAMESPACE, "xmlcache$cert_no",
             $trk_enc->{'Key'}, $trk_enc->{'KeyName'}, 128))
    {
      return undef;
    }

    $cert_no++;
  }
  
  # Sign the generated XML
  if (sign_xml($doc, KMS_XML_KM_NAMESPACE, 'xmldoc',
               $trk_enc->{'Key'}, $trk_enc->{'KeyName'}, 128))
  {
    return $doc;
  }
}

# Create KMS error response XML.
sub create_kms_error_response
{
  my $request = shift;
  my $mesg = shift;
  my $code = shift;

  # Create empty response XML and add content.
  (my $doc, my $err_node) = create_kms_response_xml($request, 1);

  $err_node->appendTextChild('ErrorCode', $code); 
  $err_node->appendTextChild('ErrorMsg', $mesg); 
  
  return $doc;
}
# For inclusion
1;
