# Functions to provide W3C XML Signing and verification and W3C Encryption and Processing 

use strict;

use XML::LibXML;
use XML::LibXML::XPathContext;
use Digest::SHA qw/sha256_base64 sha256 hmac_sha256_base64 hmac_sha256/;
use MIME::Base64;
use Crypt::OpenSSL::AES;

# W3C Namespaces
use constant W3C_NS_XMLDSIG => 'http://www.w3.org/2000/09/xmldsig#';
use constant W3C_NS_XMLDSIG_CORE => 'http://www.w3.org/2001/04/xmldsig-core#';
use constant W3C_NS_XMLENC => 'http://www.w3.org/2001/04/xmlenc#';
use constant W3C_NS_XMLC14N => 'http://www.w3.org/TR/2001/REC-xml-c14n-20010315';

# Verify the signature in an XML document.
# doc      - The XML document in DOM form.
# refns    - The name space in which to reference XML resides.
# key      - The key used to sign the signature.
# key_name - The name of the key used to sign the signature.
# Returns 1 if signature is verified, undef otherwise.
sub verify_xml
{
  my $doc = shift;
  my $refns = shift;
  my $key = shift;
  my $key_name = shift;
  
  # Register the XML signature namespace
  my $xpath_ctx = XML::LibXML::XPathContext->new($doc);
  $xpath_ctx->registerNs('xmldsig' => W3C_NS_XMLDSIG);
  $xpath_ctx->registerNs('xmlref' => $refns);
  
  # Find the URI of the reference XML and get XML at reference
  my $ref_uri = $xpath_ctx->findvalue('//xmldsig:Reference/@URI');
  # Remove # from start of ID value
  $ref_uri =~ s/^#//;
  my $ref_doc = $xpath_ctx->findnodes("//xmlref:*[\@Id='$ref_uri']") or return undef;

  # Find SHA256 of C14N XML fragment. Note sha256_base64 does not pad output which
  # is required for schema
  my $ref_hash = encode_base64(sha256($ref_doc->get_node(0)->toStringC14N()), "");

  # Check the digest matches that in the XML
  if ($ref_hash ne $xpath_ctx->findvalue("//xmldsig:DigestValue"))
  {
    print STDERR "Digest Value: " . $ref_hash . "does not match XML\n";
    return undef;
  }

  # Check we agree which key is to be used
  if ($key_name ne $xpath_ctx->findvalue('//xmldsig:KeyName'))
  {
    print STDERR "Keyname: '" . $key_name . "' does not match XML\n"; 
    return undef;
  }

  # Get SignedInfo XML and take HMAC256
  my $signed_info = $xpath_ctx->findnodes("//xmldsig:SignedInfo");
  my $sig_value = hmac_sha256($signed_info->get_node(0)->toStringC14N(), $key);
  
  # Base 64 encode required number of bits.
  my $hmac_length = $xpath_ctx->findvalue("//xmldsig:HMACOutputLength");
  if ($hmac_length % 8)
  {
    print STDERR "Output length must be multiple of 8\n"; 
    return undef;
  }
  my $hmac = encode_base64(substr($sig_value, 0, $hmac_length / 8), "");

  # Check the signature is correct
  if ($hmac ne $xpath_ctx->findvalue("//xmldsig:SignatureValue"))
  {
    print STDERR "Signature Value: " . $hmac . " does not match XML\n";
    return undef;
  }

  # XML verified
  return 1;
}

# Sign an XML document. The signature is appended to the supplied document.
# doc      - The XML document in DOM form.
# refns    - The name space in which to reference XML resides.
# ref_name - The name of the reference XML to sign.
# key      - The key used to sign the signature.
# key_name - The name of the key used to sign the signature.
# hmac_len - The length in bits of the required HMAC output.
# Returns 1 if signed, undef otherwise.
sub sign_xml
{
  my $doc = shift;
  my $refns = shift;
  my $ref_name = shift;
  my $key = shift;
  my $key_name = shift;
  my $hmac_len = shift;

  # Find the reference XML to hash
  my $xpath_ctx = XML::LibXML::XPathContext->new($doc);
  $xpath_ctx->registerNs('xmlref' => $refns);
  my $ref_doc = $xpath_ctx->findnodes("//xmlref:*[\@Id='$ref_name']") or return undef;

  # Find SHA256 of C14N XML fragment
  my $ref_hash = encode_base64(sha256($ref_doc->get_node(0)->toStringC14N()), "");

  # Create the outer Signature element at hash XML parent node
  my $sig_node = $doc->createElementNS(W3C_NS_XMLDSIG, 'Signature');
  $ref_doc->get_node(0)->parentNode->appendChild($sig_node);

  # Add Signed Info element
  my $signed_info = $doc->createElementNS(W3C_NS_XMLDSIG, 'SignedInfo');
  $sig_node->appendChild($signed_info);

  # Add Canonicalization method to SignedInfo
  append_xml_method($doc, $signed_info, W3C_NS_XMLDSIG,
                    'CanonicalizationMethod', W3C_NS_XMLC14N); 

  # Add Signature method to SignedInfo                       
  my $sig_meth = append_xml_method($doc, $signed_info, W3C_NS_XMLDSIG, 'SignatureMethod',
                                   W3C_NS_XMLDSIG_CORE . 'hmac-sha256');
  $sig_meth->appendTextChild('HMACOutputLength', $hmac_len);
                
  # Add Reference element to SignedInfo
  my $ref_node = $doc->createElementNS(W3C_NS_XMLDSIG, 'Reference');
  $ref_node->addChild($doc->createAttribute('URI' => "#$ref_name")); 
  $signed_info->appendChild($ref_node);
  
  # Add Digest method and value
  append_xml_method($doc, $ref_node, W3C_NS_XMLDSIG, 'DigestMethod',
                    W3C_NS_XMLENC . 'sha256'); 
  $ref_node->appendTextChild('DigestValue', $ref_hash);

  # Sign Info element node 
  my $sig_value = hmac_sha256($signed_info->toStringC14N(), $key);

  my $hmac = encode_base64(substr($sig_value, 0, $hmac_len / 8), "");
  $sig_node->appendTextChild('SignatureValue', $hmac);
  
  my $key_info_node = $doc->createElementNS(W3C_NS_XMLDSIG, 'KeyInfo');
  $sig_node->appendChild($key_info_node);
  $key_info_node->appendTextChild('KeyName', $key_name);

  return 1;
}

# Generate XML for key encryption.
# doc       - The XML document in DOM form to which to add the key.
# node      - The position in the document where to add the key.
# data      - The key data to encrypt.
# data_name - The name of the key data to encrypt.
# enc_key   - The key used to encrypt the key data.
# enc_name  - The name of the key used to encrypt the key data.
# Returns 1 if the encrypted key added to XML, undef otherwise.
sub encypted_key_xml 
{
  my $doc = shift;
  my $node = shift;
  my $data = shift;
  my $data_name = shift;
  my $enc_key = shift;
  my $enc_key_name = shift;

  # Check size of key is valid
  my $key_len = length($enc_key) * 8;
  if ($key_len != 128 && $key_len != 256)
  {
    print STDERR "Bad key length $key_len\n";
    return undef;
  }

  # Create the outer EncryptedKey element
  my $enc_key_node = $doc->createElementNS(W3C_NS_XMLENC, 'EncryptedKey');
  $node->appendChild($enc_key_node);
  
  # Add an encryption method
  append_xml_method($doc, $enc_key_node, W3C_NS_XMLENC, 
                    'EncryptionMethod', W3C_NS_XMLENC . "kw-aes$key_len"); 

  if ($enc_key_name ne "")
  {
    # Add key info
    my $key_info = $doc->createElementNS("", 'KeyInfo');
    $key_info->setNamespace(W3C_NS_XMLDSIG, "ds", 1);
    $enc_key_node->appendChild($key_info);

    # Add key name (have not used appendTextChild as want to control format of element name)
    my $kname = $doc->createElementNS(W3C_NS_XMLENC, 'KeyName');
    $kname->appendTextNode($enc_key_name);
    $key_info->appendChild($kname);
  }

  my $keywrap_data = keywrap_encode_aes($data, $enc_key);

  # Add Cipher data element content
  my $cipher_data = $doc->createElementNS(W3C_NS_XMLENC, 'CipherData');
  $enc_key_node->appendChild($cipher_data); 
  $cipher_data->appendTextChild('CipherValue', encode_base64($keywrap_data, ""));
  
  if ($data_name ne "")
  {
    # Add the name of the new key
    $enc_key_node->appendTextChild('CarriedKeyName', $data_name);
  }

  return 1;
}

#
# Support functions 
#

# Key wrap encode as defined by http://www.w3.org/2001/02/xmlenc#kw-aes256
# data - The key data to encrypt.
# key  - The key used to encrypt the data.
sub keywrap_encode_aes
{
  my $data = shift;
  my $key = shift;

  # Check input data is a valid size to wrap.
  if (length($data) % 8) 
  {
    die "Invalid data size";
  }

  my $blocks = length($data) / 8;
  
  # Chop input into 64-bit blocks and put in array R
  my @R; 
  for my $i (0 .. $blocks - 1)
  {
    push(@R, substr($data, $i * 8, 8) ); 
  }

  my $cipher = new Crypt::OpenSSL::AES($key);

  # Calculate intermediate values
  my $A = pack('H*', 'A6A6A6A6A6A6A6A6');
  for my $j (0 .. 5)
  {
    for my $i (0 .. $blocks - 1)
    {
      # Create t as a network order 64-bit value.
      my $t = pack('N2', (0, ($i + 1) + $j * $blocks));

      # Encrypt the block
      my $B = $cipher->encrypt($A . $R[$i]);

      $A = $t ^ substr($B, 0, 8); 
      $R[$i] = substr($B, 8, 8);
    }
  }

  # Concatenate Join all the blocks together and return result.
  return join ("", $A, @R);
}

# Append an XML Method element to specified document and node.
# doc       - The XML document in DOM form to which to add the method.
# node      - The position in the document where to add the method.
# ns        - The namespace to which the method belongs.
# name      - The name of the method element.
# algorithm - The name of the algorithm.
# Returns the XML node containing the newly added element. 
sub append_xml_method
{
  my $doc = shift;
  my $node = shift;
  my $ns = shift;
  my $name = shift;
  my $algorithm = shift;

  # Create a node to hold Method and add Algorithm attribute.
  my $new_node = $doc->createElementNS($ns, $name);
  $new_node->addChild($doc->createAttribute('Algorithm' => $algorithm)); 
  $node->appendChild($new_node);

  return $new_node;
}

# For inclusion
1;
