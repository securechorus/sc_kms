# based on https://github.com/alpha6/mojolicious-plugin-auth-helper
# modified to work with internal SQLite database with different
# schema, password hashing and session lifetime policy.

package Mojolicious::Plugin::AuthHelper;
use Mojo::Base 'Mojolicious::Plugin';

use Digest::SHA1 qw/sha1_base64/;
use UUID::Tiny;

our $VERSION = '0.04-SQLite-ajb';

sub register {
   my ($mojo, $app) = (shift, shift);

   my $dbh = $app->dbh; #Получаем дескриптор базы

   $app->helper(
      whois => sub {
         my $mojo  = shift;
         my $agent = $mojo->req->headers->user_agent || 'Unspecified User-Agent';
         my $ip    = $mojo->tx->remote_address;
         return "$agent ($ip)";
      }
   );

   $app->helper(
      check_auth => sub { #Проверяем валидность сессии
         my $mojo = shift;
         my $sid = $mojo->session('sid');
         my $sign = $mojo->whois();

         my $user = $dbh->selectrow_hashref(<<ENDSQL,undef,($sid,$sign));
            SELECT accountId AS id FROM auth_sessions
             WHERE sid = ?
               AND clientData = ?
               AND lastActivity > datetime('now', '-1 hour')
ENDSQL
         return {authorized => 0} if not $user;

         $dbh->do(<<ENDSQL);
            UPDATE auth_sessions SET lastActivity = datetime('now')
             WHERE sid = '$sid'
ENDSQL
         $user->{sid} = $sid;
         $user->{authorized} = 1;
         return $user;
      });

   $app->helper(
      login => sub { # Проверяем логин/пароль и создаем сессию
         my $mojo = shift;
         my $login = shift;
         my $pass = shift;
         my $sign = $mojo->whois();

         $pass = sha1_base64($pass) if $pass;

         my $user = $dbh->selectrow_hashref(<<ENDSQL,undef,($login,$pass));
            SELECT id FROM accounts
             WHERE name = ?
               AND sha1 = ?
ENDSQL
         if (not $user)
         {
            delete $mojo->session->{'sid'};
            return {authorized => 0};
         }

         # check for existing session
         if ($mojo->session('sid'))
         {
            if (my $existing = $mojo->check_auth())
            {
               return $existing
                   if $existing->{authorized}
                  and $user->{id} eq $existing->{id};

               $mojo->logout(undef, 1);
            }
         }

         # create new session
         my $sid = create_UUID_as_string(UUID_V4);

         $user->{sid} = $sid;

         $dbh->do(<<ENDSQL,undef,($sid, $user->{id}, $sign));
            INSERT INTO auth_sessions
               (sid, accountId, clientData) VALUES (?, ?, ?)
ENDSQL
         if ($dbh->errstr()) {
            warn $dbh->errstr();
            delete $mojo->session->{'sid'};
            return {authorized => 0};
         }

         $user->{authorized} = 1;
         $mojo->session(sid => $sid);
         return $user;
      }
   );
   $app->helper(
      logout => sub { #Удаляем сессию и куку
         my $mojo = shift;
         my $all_sessions = shift;
         my $dont_set_expiry = shift;
         my $sid = $mojo->session('sid');
         my $sign = $mojo->whois();

         if ($all_sessions)
         {
            my $uid = scalar $dbh->selectrow_array(<<ENDSQL,undef,($sid));
               SELECT accountId FROM auth_sessions
                WHERE sid = ?
ENDSQL
            $dbh->do("DELETE FROM auth_sessions WHERE accountId = $uid");
         }
         else
         {
            $dbh->do(<<ENDSQL,undef,($sid, $sign));
                DELETE FROM auth_sessions
                 WHERE sid = ?
                   AND clientData = ?
ENDSQL
         }

         $mojo->session(expires => 1 ) if not $dont_set_expiry;

         return { is_auth => 0 };
      }
   );
}

1;

# vim: ft=perlsql

